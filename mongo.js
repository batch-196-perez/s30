db.fruits.insertMany(
	[
		{
			name:"Apple",
			supplier:"Red Farm Inc.",
			stocks:20,
			price:40,
			onSale:true
		},
		{
			name:"Banana",
			supplier:"Yellow Farms",
			stocks:15,
			price:20,
			onSale:true
		},
		{
			name:"Kiwi",
			supplier:"Green Farming and Canning",
			stocks:25,
			price:50,
			onSale:true
		},
		{
			name:"Mango",
			supplier:"Yellow Farms",
			stocks:10,
			price:60,
			onSale:true
		},
		{
			name:"Dragon fruit",
			supplier:"Red Farm Inc.",
			stocks:10,
			price:60,
			onSale:true
		},

	]
)

/*
	Aggregation Pipeline Stages

	Aggregation is typically done in 2-3 steps. Each process in aggregation is called a stage.

	$match - is used to match or get document which satisfies the condition

	//syntax : {$match: {field:value}}

	$group - allows us to group together documents and creata an analysis out of the group documents.

	_id: in the group stage, essentially associates an id to our results
	_id: also determine the number of group




*/

db.fruits.aggregate([
	//look for and get all fruits that are onSale
    {$match: {onSale:true}},//apple, mango, banana, kiwi,dragon fruit

    /*
		apple
		supplier: Red Farms. Inc

		banana
		supplier
		
		$sum is used to add ot total the value of the given field

    */
    {$group: {_id:"$supplier", totalStocks:{$sum:"$stocks"}}}
    
    
    ])


db.fruits.aggregate([

	{$match: {onSale:true}},
	{$group: {_id:null, totalStocks:{$sum:"$stocks"}}}


])

db.fruits.aggregate([

	{$match: {onSale:true}},
	{$group: {_id:"AllOnSaleFruits", totalStocks:{$sum:"$stocks"}}}


])


db.fruits.aggregate([

	{$match: {supplier:"Red Farm Inc."}},
	{$group: {_id:"AllOnSaleFruits", totalStocks:{$sum:"$stocks"}}}


])

db.fruits.aggregate([

	{$match: {onSale:true, supplier:"Yellow Farms"}},
	{$group: {_id:"YellowFarm", totalStocks:{$sum:"$stocks"}}}


])

// $avg - is an operator used in $group stage
// $avg - gets the avg of the numerical values

db.fruits.aggregate([

	{$match:{onSale:true}},
	{$group:{_id:"$supplier", avgStock:{$avg:"$stocks"}}}

]) 

db.fruits.aggregate([

	{$match:{onSale:true}},
	{$group:{_id:null,avgPrice: {$avg:"$price"}}}

])

//$max - will allow us to get the highest value out of the value in a given field per group

// highest number of stock for all items on sale 
db.fruits.aggregate([
	{$match:{onSale:true}},
	{$group:{_id:"highestStockOnSale",maxStock : {$max: "$stocks"}}}
])

// highest price for all items on sale
db.fruits.aggregate([

	{$match: {onSale:true}},
	{$group: {_id:null, maxPrice: {$max: "$price"}}}
])

// $min - will allow us to get the lowest value out of the value in a given field group

//get the lowest number of stock for all items on sale
db.fruits.aggregate([

	{$match:{onSale:true}},
	{$group:{_id:"lowestStockOnSale", minStock: {$min: "$stocks"}}}

])

db.fruits.aggregate([

	{$match:{onSale:true}},
	{$group:{_id:"lowestPriceOnSale", minPrice: {$min: "$price"}}}

])

db.fruits.aggregate([

	{$match:{price:{$lt:50}}},
	{$group:{_id:"lowestStock", minStock: {$min: "$stocks"}}}

])


// Other Stages
//$count - is a stage added after $match stage to count all items that matches our criteria


db.fruits.aggregate([

	{$match: {onSale:true}},
	{$count: "itemsOnsale"}

	])


db.fruits.aggregate([

	{$match: {price:{$lt:50}}},
	{$count: "itemsPriceLessThan50"}

	])


//$out - save/ output the result in a new collection

db.fruits.aggregate([

	{$match: {onSale:true}},
	{$group: {_id:"$supplier", avgPrice: {$avg:"$price"}}},
	{$out: "stocksPerSupplier"}


])