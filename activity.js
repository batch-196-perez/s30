//1.

db.fruits.aggregate([

	{$match: {supplier:"Yellow Farms", price:{$lt:50}}},
	{$count: "YellowFarmitemsPriceLessThan50"}


//2. 
])
db.fruits.aggregate([

	{$match: {price:{$lt:30}}},
	{$count: "totalNumberofItemsLessThan30"}

])


//3.
db.fruits.aggregate([

	{$match: {supplier:"Yellow Farms"}},
	{$group: {_id:"averagePrice",avgPriceYellowFarm: {$avg :"$price"}}}

])


//4.
db.fruits.aggregate([

	{$match: {supplier:"Red Farm Inc."}},
	{$group: {_id: "highestPrice", highestPrice: {$max: "$price"}}}


])


//5.
db.fruits.aggregate([

	{$match: {supplier:"Red Farm Inc."}},
	{$group: {_id: "lowestPrice", lowesttPrice: {$min: "$price"}}}


])